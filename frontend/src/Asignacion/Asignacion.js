import React from "react";

const Asignacion = () => {

    const handleSubmit = (e) => {
        e.preventDefault();
        const requestOptions = {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({
                id: e.target.id.value,
                student: e.target.student.value,
                subject: e.target.subject.value,
                section: e.target.section.value,
                day: e.target.day.value,
                hour: e.target.hour.value,
            })
        };
        fetch('http://localhost:8080/asignar', requestOptions)
            .then((response) => response.json())
            .then((data) => {
                alert(data.result)
            });
        e.target.reset();
    };

    return (
        <div className="div__Asignacion">
            <h1>Asignación</h1>
            <form onSubmit={handleSubmit} >
                <div className="input-container">
                    <label>ID Curso </label>
                    <input type="text" id="id" name="id" />
                </div>
                <div className="input-container">
                    <label>Username </label>
                    <input type="text" id="student" name="student" />
                </div>
                <div className="input-container">
                    <label>Curso </label>
                    <input type="text" id="subject" name="subject" />
                </div>
                <div className="input-container">
                    <label>Sección </label>
                    <input type="text" id="section" name="section" />
                </div>
                <div className="input-container">
                    <label>Día </label>
                    <input type="text" id="day" name="day" />
                </div>
                <div className="input-container">
                    <label>Hora </label>
                    <input type="text" id="hour" name="hour" />
                </div>
                <div className="button-container">
                    <input type="submit" value={"Asignarse"} />
                </div>
            </form>
        </div>
    )
}

export default Asignacion;