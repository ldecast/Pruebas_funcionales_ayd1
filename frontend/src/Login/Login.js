import React from "react";

const Login = () => {

    const handleSubmit = (e) => {
        e.preventDefault();
        const requestOptions = {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({
                email: e.target.email.value,
                password: e.target.password.value,
            })
        };
        fetch('http://localhost:8080/login', requestOptions)
            .then((response) => response.json())
            .then((data) => {
                alert(data.result)
            });
        e.target.reset();
    };

    return (
        <div className="div__Login">
            <h1>Login</h1>
            <form onSubmit={handleSubmit} >
                <div className="input-container">
                    <label>Correo </label>
                    <input type="email" id="email" name="email" />
                </div>
                <div className="input-container">
                    <label>Contraseña </label>
                    <input type="password" id="password" name="password" />
                </div>
                <div className="button-container">
                    <input type="submit" value={"Ingresar"} />
                </div>
            </form>
        </div>
    )
}

export default Login;