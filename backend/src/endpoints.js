const express = require('express');

const router = express.Router();

let students = []
let assignments = [];

router.get('/', (req, res) => {
    res.send('Hello from Node Server!');
});

router.post('/registro', (req, res) => {
    try {
        const body = req.body;
        if (body.id && body.name && body.surname && body.email && body.date && body.password && body.confirm_password) {
            let x = false;
            for (let i = 0; i < students.length; i++) {
                const student = students[i];
                if (student.id == body.id || student.email == body.email) {
                    res.status(403).json({ result: 'Usuario ya existe, intente de nuevo' });
                    x = true;
                    return;
                }
            }
            if (!x) {
                if (body.password == body.confirm_password) {
                    students.push(body);
                    res.status(201).json({ result: 'Estudiante registrado' });
                }
                else {
                    res.status(400).json({ result: 'Las contraseñas no coinciden' });
                }
            }
        }
        else
            res.status(422).json({ result: 'Parámetros incompletos, reintente nuevamente' });
    } catch (error) {
        console.log('ERROR: ', error);
        res.sendStatus(500);
    }
});

router.post('/login', (req, res) => {
    try {
        const body = req.body;
        if (body.email && body.password) {
            let x = false;
            for (let i = 0; i < students.length; i++) {
                const student = students[i];
                if (student.email == body.email) {
                    if (student.password == body.password) {
                        x = true;
                        res.status(202).json({ result: 'Sesión iniciada correctamente' });
                        return;
                    }
                    else {
                        x = true;
                        res.status(401).json({ result: 'Contraseña incorrecta, intente de nuevo' });
                        return;
                    }
                }
            }
            if (!x) {
                res.status(403).json({ result: 'Usuario no existe, debe registrarse' });
                return;
            }
        }
        else
            res.status(422).json({ result: 'Parámetros incompletos, reintente nuevamente' });
    } catch (error) {
        console.log('ERROR: ', error);
        res.sendStatus(500);
    }
});

router.post('/asignar', (req, res) => {
    try {
        const body = req.body;
        if (body.id && body.student && body.subject && body.section && body.day && body.hour) {
            let x = false;
            students.forEach(student => {
                if (student.id == body.student)
                    x = true;
            });
            if (x) {
                assignments.push(body);
                res.status(201).json({ result: 'Asignación de curso registrada' });
            }
            else
                res.status(401).json({ result: 'Estudiante no existe, reintente nuevamente' });
        }
        else
            res.status(422).json({ result: 'Parámetros incompletos, reintente nuevamente' });
    } catch (error) {
        console.log('ERROR: ', error);
        res.sendStatus(500);
    }
});

router.get('/estudiantes', (req, res) => {
    res.status(200).json({ estudiantes: students });
});

router.get('/asignaciones', (req, res) => {
    res.status(200).json({ asignaciones: assignments });
});

module.exports = router;