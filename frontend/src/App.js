import './App.css';
import Asignacion from './Asignacion/Asignacion';
import Login from './Login/Login';
import Registro from './Registro/Registro';

function App() {
  return (
    <div className="container-fluid fondo">
      <div className='row m-5'>
        <div className='col-4'>
          <Registro />
        </div>
        <div className='col-4'>
          <Login />
        </div>
        <div className='col-4'>
          <Asignacion />
        </div>
      </div>
      <hr />
    </div>
  );
}

export default App;
