import React from "react";

const Registro = () => {

    const handleSubmit = (e) => {
        e.preventDefault();
        const requestOptions = {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({
                id: e.target.username.value,
                name: e.target.nombres.value,
                surname: e.target.apellidos.value,
                email: e.target.correo.value,
                date: e.target.fnac.value,
                password: e.target.password.value,
                confirm_password: e.target.cpassword.value,
            })
        };
        fetch('http://localhost:8080/registro', requestOptions)
            .then((response) => response.json())
            .then((data) => {
                alert(data.result)
            });
        e.target.reset();
    };

    return (
        <div className="div__registro">
            <h1>Registro</h1>
            <form onSubmit={handleSubmit} >
                <div className="input-container">
                    <label>Username </label>
                    <input type="text" id="username" name="username" />
                </div>
                <div className="input-container">
                    <label>Nombres </label>
                    <input type="text" id="nombres" name="nombres" />
                </div>
                <div className="input-container">
                    <label>Apellidos </label>
                    <input type="text" id="apellidos" name="apellidos" />
                </div>
                <div className="input-container">
                    <label>Correo </label>
                    <input type="email" id="correo" name="correo" />
                </div>
                <div className="input-container">
                    <label>Fecha de nacimiento </label>
                    <input type="date" id="fnac" name="fnac" />
                </div>
                <div className="input-container">
                    <label>Contraseña </label>
                    <input type="password" id="password" name="password" />
                </div>
                <div className="input-container">
                    <label>Confirmar contraseña </label>
                    <input type="password" id="cpassword" name="cpassword" />
                </div>
                <div className="button-container">
                    <input type="submit" value={"Registrarse"} />
                </div>
            </form>
        </div>
    )
}

export default Registro;